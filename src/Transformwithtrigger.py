import sensor_msgs.point_cloud2 as pc2
import rospy
from math import atan2
from sensor_msgs.msg import PointCloud2, LaserScan
import laser_geometry.laser_geometry as lg
import math
import numpy as np
import tf
import tf2_geometry_msgs
import tf2_ros
from collections import deque
from tf import TransformListener
import time
#from beginner_tutorials.srv import *
from laser2PC.srv import *
from laser2PC.srv import Parking
from laser2PC.srv import Reverse
import sensor_msgs.point_cloud2 as pcl2
from nav_msgs.msg import Odometry
import std_msgs.msg
import tf2_ros
import tf2_msgs.msg
import geometry_msgs.msg
from math import pow
from math  import sqrt, asin
from std_msgs.msg import Float64MultiArray
from nav_msgs.msg import Odometry , Path
from geometry_msgs.msg import Point, Twist ,PoseStamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from math import pi
from math import sin
from math import cos
from math import tan
import message_filters


rospy.init_node("laserscan_to_pointcloud")

lp = lg.LaserProjection()

pc_pub = rospy.Publisher("converted_pc", PointCloud2, queue_size=1)
odom_pub = rospy.Publisher("/odom", Odometry, queue_size=1)
path_publisher = rospy.Publisher('carpath', Path, queue_size=5)
twistpub = rospy.Publisher("/control/keyboard", Twist, queue_size=1)
twistsub = rospy.Subscriber("/control/keyboard", Twist, queue_size=1)

#path_publisher = rospy.Publisher('carpath', Path, queue_size=5)

tf_buffer=tf2_ros.Buffer(rospy.Duration(1200.0))
tf_listener=tf2_ros.TransformListener(tf_buffer)
scanbuffer = list()

tf3 = TransformListener()
listener = tf.TransformListener()

global offset
offset=[]

#begintime=time.time()
global flag2
flag2=False
global result
result="Nothing Found"
global contour
contour = []

global destinationpoints
destinationpoints = []

global initial
initial = []

global freespacecoordinates
freespacecoordinates=[]

global countflag
countflag=0

global frontbumpercoordinates
frontbumpercoordinates=[]

global rearbumpercoordinates
rearbumpercoordinates=[]

global updatedcontour
updatedcontour = []

global pointsofinterest
pointsofinterest = []

global shift
shift = []

global Ynewshift
Ynewshift = 0.0

global yawfinal
yawfinal = 0.0

global Px
Px = 0.0

global x
x = 0.0
global y
y = 0.0
global th
th = 0.0
global sum
sum= 0
global count
count = 0
global reverse
reverse = False
global locationarray
locationarray = []
global emergencybrake
emergencybrake = False
global turningpoint
turningpoint = False
global park
park = False
global status
status = ""
global Horizontaldistance
Horizontaldistance = []
global Xnew
Xnew = 0.0
global Ynew
Ynew=0.0
global cardrifting
cardrifting = 0.0
global Yfootprint2
Yfootprint2 = 0.0
global xt
xt = 0.0
global yt
yt = 0.0
global depth
depth = 0.0
global studypoints
studypoints = []
global frontbumperwidth
frontbumperwidth = 0.0
global centrofbumper
centrofbumper = 0.0
global threshold
threshold = []

global begintime
begintime=time.time()

current_time = rospy.Time.now()
global last_time
last_time = rospy.Time.now()
br = tf.TransformBroadcaster()


def main():
    print("Hell of Parking ...so far")
    scan_sub = rospy.Subscriber("/scan", LaserScan,scan_cb , queue_size=1)
    #rpm_sub = rospy.Subscriber("/rccar_movement/rpm", Float64MultiArray,rpm_cb , queue_size=1)

    s = rospy.Service('Park', Parking, handle_parking)
    r = rospy.Service('Reverse', Reverse, handle_reverse)

    rospy.spin()

def rpm_cb(msg):
        global sum
        global count
        global locationarray

        global last_time
        global x
        global y
        global th
        print("ENTERED")

        vR = msg.data[1]
        vL = msg.data[0]
        b = 0.34
        vRback = msg.data[3]
        vLback = msg.data[2]

        speed = (vRback + vLback) / 2.0
        speed = speed / 60 * pi * 0.115
        vth = (vR - vL) / b
        deltav = vR - vL
        # vth = vth /60 * pi * 0.117

        # vx = 0.1
        # vy = -0.0
        # vth = 0

        vx = speed
        vy = 0.0
        vth = vth / 60 * pi * 0.117

        if (deltav != 0):
            R = speed / vth
            sum = sum + R
            count=count + 1
            Avg = sum/count
            print(" Avg : %f " % Avg)
            # R = speed * 0.34 / deltav
            if (abs(Avg) > 0.53):
                print(" R : %f " % R)
                Ig = pi / 2 / (asin(0.53 / Avg))
                # print(" Ig : %f " % Ig)
                Ig = 90 / Ig
                print(" steering angle : %f " % Ig)

        current_time = rospy.Time.now()
        dt = (current_time - last_time).to_sec()
        delta_x = (vx * cos(th) - vy * sin(th)) * dt
        delta_y = (vx * sin(th) + vy * cos(th)) * dt
        delta_th = vth * dt
        #
        x += delta_x
        y += delta_y
        th += delta_th
        #
        locationarray.append([x,y,0])
        # odom_quat = tf.transformations.quaternion_from_euler(0, 0, th)
        #
        # br.sendTransform((x, y, 0),
        #                   odom_quat,
        #                  rospy.Time.now(),
        #                  "base_footprint",
        #                  "map")
        # br.sendTransform((0.64, -0.12, 0.25),
        #                  (0.0, 0.0, 0.706825, 0.70739),
        #                  rospy.Time.now(),
        #                  "laser",
        #                  "base_footprint")
        #
        # odom = Odometry()
        # odom.header.stamp = current_time
        # odom.header.frame_id = "map"
        #
        # # set the position
        # odom.pose.pose = Pose(Point(x, y, 0.), Quaternion(*odom_quat))
        # # odom_sub.
        # # set the velocity
        # odom.child_frame_id = "base_footprint"
        # odom.twist.twist = Twist(Vector3(vx, vy, 0), Vector3(0, 0, vth))
        # # publish the message
        # odom_pub.publish(odom)
        #
        header = std_msgs.msg.Header()
        header.stamp = rospy.Time.now()
        header.frame_id = 'map'

        scaled_polygon_pcl = pcl2.create_cloud_xyz32(header, locationarray)
        pc_pub.publish(scaled_polygon_pcl)
        #print(len(locationarray))
        print(" x : %f " % x)
        print(" y : %f " % y)


        #
        last_time = current_time

def handle_parking(req):
    global Px
    global offset
    global contour
    global flag2
    flag2 = True
    if(len(offset) >0):
        return result

def handle_reverse(req):
    global begintime
    global status
    global reverse
    begintime = time.time()
    reverse = True
    return status
def scan_cb(msg):
    global threshold
    global centrofbumper
    # print(" 0 : %f " % msg.ranges[0])
    # print(" 90 : %f " % msg.ranges[90])
    # print(" 180 : %f " % msg.ranges[180])
    # print(" 270 : %f " % msg.ranges[270])
    # print(" 360 : %f " % msg.ranges[359])
    #print(len(msg.ranges))
    #print(msg.ranges)
    #print(msg.ranges[0])
    global depth
    global xt
    global yt
    global Yfootprint2
    global cardrifting
    global begintime
    # speed = Twist()
    global destinationpoints
    global initial
    global status
    global park
    global turningpoint
    global emergencybrake
    global yawfinal
    global ynew2
    global shift
    global Ynewshift
    global roll, pitch, yaw
    global freespacecoordinates
    global contour
    global flag2
    global countflag

    global frontbumpercoordinates
    global rearbumpercoordinates
    global updatedcontour
    global pointsofinterest
    global reverse
    global Horizontaldistance
    print("kkg")

    for x in range( 0, 90, 1):
      if(abs(msg.ranges[x] - msg.ranges[x+1] <3 )):
          if(msg.ranges[x] < msg.ranges[x+1]):
              min = msg.ranges[x]
          else:
              min = msg.ranges[x+1]

          b = (math.sqrt(2*(1 - cos(msg.angle_increment))) * min)
          #b = (msg.ranges[x] - msg.ranges[x+1]) / (msg.ranges[x] + msg.ranges[x+1])
          threshold.append(b)
    #print(threshold)
    #print(len(threshold))
    print(np.average(threshold))
    if(countflag ==2 and reverse == True):

            pc2_msg = lp.projectLaser(msg)
            pc_pub.publish(pc2_msg)
            point_generator = pc2.read_points(pc2_msg)
            point_list = pc2.read_points_list(pc2_msg)
            pointer = 0
            speed = Twist()

            while (pointer < len(point_list)):
                # if(turningpoint == True and -0.2 < point_list[pointer].x < -0.1 and 0 < point_list[pointer].y < 0.8):
                #     print("EmergencyStoprightside")
                #     speed.linear.x = 0.0
                #     speed.angular.z = 0.0
                #     twistpub.publish(speed)
                #     emergencybrake = True


                if (0 < point_list[pointer].x < 0.5 and -0.01 < point_list[pointer].y < 0.01):
                    print("EmergencyStop")
                    speed.linear.x = 0.0
                    speed.angular.z = 0.0
                    twistpub.publish(speed)
                    emergencybrake = True

                pointer += 1

            try:
                t = tf3.getLatestCommonTime("/base_footprint", "/map")
                position, quaternion = tf3.lookupTransform("/base_footprint", "/map", t)
                (roll, pitch, yaw) = euler_from_quaternion(quaternion)
                X = (-1) * position[0]
                Y = (-1) * position[1]
                Xnew = (X * cos(yaw)) + (Y * sin(yaw))
                Ynew = (-1) * (X * sin(yaw)) + (Y * cos(yaw))
                # print("yaw")
                # print(yaw)
                #print("Auto")
                print(" Xnew : %f " % Xnew)
                print(" Ynew : %f " % Ynew)
                print(" Orientation : %f " % yaw)
                # print(Xnew)
                # print(turningpoint)
                # print(count)
                # print(Ynew)
            except Exception as e:
                rospy.loginfo("Autoerror")
            i = 0
            print(" distance to steer right : %f " % (Xnew - initial[0][0]))
            # print(Xnew - initial[0][0])
            print(" distance to stop : %f " % (Xnew - destinationpoints[0][0]))
            #print(" Emergency brake ")
            print(emergencybrake)
            print(" xt : %f " % xt)
            print(" yt : %f " % yt)

            #print(Xnew - destinationpoints[0][0])
            if (Xnew > initial[0][0] + 0.1 and emergencybrake == False):
                print("reverse")
                speed.linear.x = -0.3
                speed.angular.z = 0.0
                twistpub.publish(speed)
            elif (turningpoint == False and emergencybrake == False):
                speed.linear.x = -0.3
                speed.angular.z = 90.0
                twistpub.publish(speed)
                print("Right")
                # print(Xnew)
                # park = Truew
            #and Ynew < yt
            if (Xnew < xt + 0.1  and Ynew < yt + 0.1  and emergencybrake == False and turningpoint == False):

                turningpoint = True
            # print(position)w
            # print(destinationpoints)
            print(time.time() - begintime)
            i = 0

            if (turningpoint == True and Xnew > destinationpoints[0][0] and emergencybrake == False):
                print("Left")
                speed.linear.x = -0.3
                speed.angular.z = -90.0
                twistpub.publish(speed)
            if(turningpoint == True and (Ynew -centrofbumper) < -0.05):
                emergencybrake = True
                speed.linear.x = 0.0
                speed.angular.z = 0.0
                twistpub.publish(speed)
                print("car will collide with pavement")
            if (turningpoint == True and ( Xnew < (destinationpoints[0][0]) or -0.05<yaw<0.05 or Xnew < frontbumpercoordinates[len(frontbumpercoordinates)-1][0] + 0.1) and emergencybrake == False):
                emergencybrake = True
                print("Stop")
                # print(destinationpoints[0][0])
                speed.linear.x = 0.0
                speed.angular.z = 0.0
                twistpub.publish(speed)
            # if ((time.time() - bwegintime > 20)):
            #     emergencybrake = True
            #     print("EmergencyStopTime")
            #     speed.linear.x = 0.0
            #     speed.angular.z = 0.0
            #     twistpub.publish(speed)

    if(countflag==1 and result == "Front Car found and Free space" ):
        print(result)
        publish()
    if(flag2==True and countflag==0):
        #global begintime

        #if(time.time()-begintime >0.25):
         #   begintime=time.time()
        #else:
         #   return
        # convert the message of type LaserScan to a PointCloud2
        pc2_msg = lp.projectLaser(msg)
        pc_pub.publish(pc2_msg)
        point_generator = pc2.read_points(pc2_msg)
        point_list = pc2.read_points_list(pc2_msg)
        print(np.average(threshold))

        try:
            t = tf3.getLatestCommonTime("/base_footprint", "/map")
            shift, quaternion = tf3.lookupTransform("/base_footprint", "/map", t)
            (roll, pitch, yawgeneric) = euler_from_quaternion(quaternion)
            print(" yawgeneric) : %f " % yawgeneric)
            print(" shift[0] : %f " % shift[0])
            print(" shift[1] : %f " % shift[1])

            X = (-1) * shift[0]
            Y = (-1) * shift[1]
            Xfootprint2 = (X * cos(yawgeneric)) + (Y * sin(yawgeneric))
            Yfootprint2 = (-1) * (X * sin(yawgeneric)) + (Y * cos(yawgeneric))
            print(" Xfootprint : %f " % Xfootprint2)
            print(" Yfootprint : %f " % Yfootprint2)

            #print(" base_footprint : %f " % shift)
            #print("yaw")
            #print(yawgeneric)
            #X = (-1) * shift[0]
            #Y = (-1) * shift[1]
            #Xnewshift = (X * cos(yaw)) + (Y * sin(yaw))
            #Ynewshift = (-1) * (X * sin(yaw)) + (Y * cos(yaw))

        except Exception as e:
            rospy.loginfo("calculatef")

        try:
            t = tf3.getLatestCommonTime("/laser", "/map")
            position, quaternion = tf3.lookupTransform("/laser", "/map", t)

            (roll, pitch, yaw) = euler_from_quaternion(quaternion)
            offset.append(position)
            #print("yaw")
            #print(yaw)
            # print(yaw)
            # print(position)
            #rate.sleep()
            #cn= position[0]+point_list[0].x
            #test3.append(position)
            #print(cn)
            if(len(offset) <2):

                pointer2 = 0
                testarray = []
                while (pointer2 < len(point_list)):
                    if(-3 < point_list[pointer2].y < 0 and point_list[pointer2].x < 0):

                        # X = point_list[pointer2].x - position[0]
                        # Y = point_list[pointer2].y - position[1]
                        #
                        # Ynew = (-1) * (X * sin(yaw)) + (Y * cos(yaw))

                        testarray.append(point_list[pointer2].x)
                    pointer2 += 1
                print(testarray)
                print(np.average(testarray))
                print(" depth : %f " % (np.average(testarray)))
                depth = 3 * np.average(testarray)

            print(" parkingdepthscan : %f " % (depth))
            pointer = 0
            while (pointer < len(point_list)):

                X = point_list[pointer].x - position[0]
                Y = point_list[pointer].y - position[1]

                Xnew = (X * cos(yaw)) + Y * sin((yaw))
                Ynew = (-1) * (X * sin(yaw)) + (Y * cos(yaw))

                # if (point_list[pointer].x < depth and -0.8 < point_list[pointer].y < 0 and len(rearbumpercoordinates) == 0 ):
                #     contour.append([Xnew,Ynew,0])

                if(depth < point_list[pointer].x < -0.1 and (abs(offset[0][1] - offset[len(offset) - 1][1])) <1 and 0 < point_list[pointer].y < 1.3):

                    frontbumpercoordinates.append([Xnew,Ynew,0])
                    pointsofinterest.append([Xnew,Ynew,0])

                if (depth < point_list[pointer].x < 0 and 1<(abs(offset[0][1] - offset[len(offset) - 1][1]))  and -1.3 < point_list[pointer].y < 0):
                    #Horizontaldistance = abs(point_list[pointer].x)

                    rearbumpercoordinates.append([Xnew, Ynew, 0])
                    pointsofinterest.append([Xnew,Ynew,0])

                if (depth< point_list[pointer].x < 0 and (abs(offset[0][1] - offset[len(offset) - 1][1])) < 1 and -0.01 < point_list[pointer].y < 0.01):
                    Horizontaldistance.append([Xnew, Ynew, 0])
                    #pointsofinterest.append([Xnew, Ynew, 0])

                pointer += 1

            print(abs(offset[0][1] - offset[len(offset) - 1][1]))
            #print(abs(offset[0][0] - offset[len(offset) - 1][0]))

            if(abs(offset[0][1] - offset[len(offset) - 1][1]) > 2):
            #if((abs(offset[0][1] - offset[len(offset) - 1][1])) > 3):
                flag2=False
                countflag=1
                try:
                    t = tf3.getLatestCommonTime("/base_footprint", "/map")
                    shift, quaternion = tf3.lookupTransform("/base_footprint", "/map", t)
                    (roll, pitch, yawfinal) = euler_from_quaternion(quaternion)
                    X = (-1) * shift[0]
                    Y = (-1) * shift[1]
                    Xnewshift = (X * cos(yawfinal)) + (Y * sin(yawfinal))
                    Ynewshift = (-1) * (X * sin(yawfinal)) + (Y * cos(yawfinal))

                except Exception as e:
                    rospy.loginfo("calculate3")
                #print(contour)
                #print(len(contour))
                #print(len(contour))
                #print(frontbumpercoordinates[0])
                #print (len(frontbumpercoordinates[0]))
                print ("ii")

                #checkforspace()
                bumpertwist()

        except Exception as e:
            rospy.loginfo("calculate")

def checkforspace():
    global centrofbumper
    global frontbumperwidth
    global rearbumpercoordinates
    global frontbumpercoordinates
    global result
    global freespacecoordinates
    global status

    i = 0
    sumy = 0
    sumx = 0
    global studypoints

    i = 0

    print("FreeSpace")
    FreeSpace = abs(frontbumpercoordinates[len(frontbumpercoordinates) - 1][0] - rearbumpercoordinates[0][0])
    print(abs(frontbumpercoordinates[len(frontbumpercoordinates) - 1][0] - rearbumpercoordinates[0][0]))

    if(abs(frontbumpercoordinates[len(frontbumpercoordinates)-1][0] - rearbumpercoordinates[0][0] )> 1.3):
        print(abs(frontbumpercoordinates[len(frontbumpercoordinates)-1][0] - rearbumpercoordinates[0][0]))
        #Horizontal sorting
        for x in range(len(frontbumpercoordinates) - 1, 0, -1):
            for y in range(x):
                if frontbumpercoordinates[y][1] > frontbumpercoordinates[y + 1][1]:
                    temp = frontbumpercoordinates[y]
                    frontbumpercoordinates[y] = frontbumpercoordinates[y + 1]
                    frontbumpercoordinates[y + 1] = temp

        frontbumperwidth = frontbumpercoordinates[len(frontbumpercoordinates) - 1][1] - frontbumpercoordinates[0][1]
        print(" frontbumperwidth : %f " % (frontbumperwidth))

        if(frontbumperwidth > 0.35):
            result = "Front Car found and Free space"
            status = "Length of free space is  " + str(FreeSpace)
            centrofbumper = (frontbumpercoordinates[len(frontbumpercoordinates) - 1][1] + frontbumpercoordinates[0][1] )/2
            #centretest = frontbumpercoordinates[len(frontbumpercoordinates/2)-1][1]
            #print(" centretest : %f " % (centretest))
            print(" centreofbumper : %f " % (centrofbumper))
    else:
        result = "Failure"
        print("Failure")


def bumpertwist():
    global threshold
    global result
    global updatedcontour
    global contour
    global frontbumpercoordinates
    global rearbumpercoordinates
    global depth
    print(len(frontbumpercoordinates))
    print(len(rearbumpercoordinates))
    sum = 0
    for x in range(len(frontbumpercoordinates) - 1, 0, -1):
       sum += frontbumpercoordinates[x][1]
    Averagedepth = sum/len(frontbumpercoordinates)
    print(" The Average depth : %f " % (Averagedepth))

    # for x in range(len(frontbumpercoordinates)-1 , 0, -1):
    #     if(abs(frontbumpercoordinates[x][1] - depth) <0.1):
    #         del(frontbumpercoordinates[x])
    # for x in range(len(rearbumpercoordinates) -1, 0, -1):
    #     if(abs(rearbumpercoordinates[x][1] - depth) <0.1):
    #         del(rearbumpercoordinates[x])
    #verticalsorting
    for x in range(len(frontbumpercoordinates) - 1, 0, -1):
        for y in range(x):
            if frontbumpercoordinates[y][0] > frontbumpercoordinates[y + 1][0]:
                temp = frontbumpercoordinates[y]
                frontbumpercoordinates[y] = frontbumpercoordinates[y + 1]
                frontbumpercoordinates[y + 1] = temp
    x = 0
    while(x< len(frontbumpercoordinates) - 1):
#or (abs(frontbumpercoordinates[x][1] - Averagedepth))> 0.7
        if ((frontbumpercoordinates[x+1][0] - frontbumpercoordinates[x][0] ) > np.average(threshold) or (abs(frontbumpercoordinates[x][1] - Averagedepth))> 0.4):
           print((frontbumpercoordinates[x+1][0] - frontbumpercoordinates[x][0] ))
           del(frontbumpercoordinates[0: x+1])
           print("deleted")
           x = -1
        x += 1
    # verticalsorting
    #x = 0
    for x in range(len(rearbumpercoordinates) - 1, 0, -1):
        for y in range(x):
            if rearbumpercoordinates[y][0] > rearbumpercoordinates[y + 1][0]:
                temp = rearbumpercoordinates[y]
                rearbumpercoordinates[y] = rearbumpercoordinates[y + 1]
                rearbumpercoordinates[y + 1] = temp

    # x = 0
    # while (x < len(rearbumpercoordinates) - 1):
    #     if ((rearbumpercoordinates[x + 1][0] - rearbumpercoordinates[x][0]) > 0.008 (abs(rearbumpercoordinates[x][1] - Averagedepth))> 0.2):
    #         print((rearbumpercoordinates[x + 1][0] - rearbumpercoordinates[x][0]))
    #         del (rearbumpercoordinates[0: x+1])
    #         x = -1
    #     x += 1

    print("testing")
    print(frontbumpercoordinates[len(frontbumpercoordinates) - 1])

    # header = std_msgs.msg.Header()
    # header.stamp = rospy.Time.now()
    # header.frame_id = 'map'
    # scaled_polygon_pcl = pcl2.create_cloud_xyz32(header, rearbumpercoordinates)
    # pc_pub.publish(scaled_polygon_pcl)
    #print(rearbumpercoordinates)
    print(rearbumpercoordinates[0])
    result = "Front Car"
    checkforspace()
def publish():
    global centrofbumper
    global xt
    global yt
    global Yfootprint2
    global y
    global cardrifting
    global yawfinal
    #global ynew2
    global shift
    global Ynewshift
    global offset
    global initial
    global countflag
    global destinationpoints
    global frontbumpercoordinates
    global rearbumpercoordinates
    global freespacecoordinates
    global pointsofinterest
    global Horizontaldistance
    global frontbumperwidth

    sum = 0
    i = 0
    while(i<len(Horizontaldistance)):
        sum = sum+Horizontaldistance[i][1]
        i+=1
    frontbumperdepth = sum/len(Horizontaldistance)

    #print(" frontbumperdepth : %f " % (frontbumperdepth))
    #print(frontbumpercoordinates)

    header = std_msgs.msg.Header()
    header.stamp = rospy.Time.now()
    header.frame_id = 'map'
    #scaled_polygon_pcl = pcl2.create_cloud_xyz32(header, pointsofinterest)
    #path_publisher = rospy.Publisher('carpath', Path, queue_size=5)
    pathofcar = Path()
    pathofcar.header.frame_id = "map"
    pathofcar.header.stamp = rospy.Time.now()
    # verticalsorting
    # for x in range(len(rearbumpercoordinates) - 1, 0, -1):
    #     for y in range(x):
    #         if rearbumpercoordinates[y][1] > rearbumpercoordinates[y + 1][1]:
    #             temp = rearbumpercoordinates[y]
    #             rearbumpercoordinates[y] = rearbumpercoordinates[y + 1]
    #             rearbumpercoordinates[y + 1] = temp
    # verticalsorting
    for x in range(len(frontbumpercoordinates) - 1, 0, -1):
        for y in range(x):
            if frontbumpercoordinates[y][0] > frontbumpercoordinates[y + 1][0]:
                temp = frontbumpercoordinates[y]
                frontbumpercoordinates[y] = frontbumpercoordinates[y + 1]
                frontbumpercoordinates[y + 1] = temp

    # rearbumperwidth = rearbumpercoordinates[len(rearbumpercoordinates)-1][1] - rearbumpercoordinates[0][1]
    # print(" rearbumperwidth : %f " % (rearbumperwidth))

    #width = get horizontal line for the bumper and measure it's depth , get centre point of this line
    width = 0.4
    carlength=0.81
    rearaxletoendofcar=0.15
    r = 1.5

    # h = freespacecoordinates[0][0][0] + rearaxletoendofcar
    # k = freespacecoordinates[0][0][1] + 0.05 + 0.5 * width + r
    h = frontbumpercoordinates[len(frontbumpercoordinates)-1][0] + 0.15
    #h = freespacecoordinates[0][0] + rearaxletoendofcar
    k = centrofbumper + r

    xc1 = h
    yc1 = k


    k2 = (Ynewshift) - r
    yc2 = k2
    yt = (yc1 + yc2) / 2
    xt = xc1 + sqrt(pow(1.5, 2) - (pow((yt - yc1), 2)))
    xc2 = 2 * xt - xc1
    print(" xc2 : %f " % (xc2))
    print(" yt : %f " % (yt))
    print(" xt : %f " % (xt))
    #k = Ynewshift + r
    print(" h : %f " % (h))
    print(" k : %f " % (k))
    print(" cardrifting : %f " % (cardrifting))
    print(" carlocationoffset : %f " % (Yfootprint2))

    pointsofinterest.append([h,k,0])
    x = h
    while (-r < x - h < r ):
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = x
        y = (-1) * (math.sqrt(pow(r, 2) - pow((x - h), 2)))
        y = y + k
        pose.pose.position.y = y
        if(pose.pose.position.x<= xt):
            pathofcar.poses.append(pose)
            destinationpoints.append([pose.pose.position.x , pose.pose.position.y])
        x += 0.1
   #yoffset=abs(rearbumpercoordinates[0][1] - freespacecoordinates[0][len(freespacecoordinates[0])-1][1])
    print(x)
    print("x")
    #print(yoffset)
    print("shift")
    print(Ynewshift)
    print(shift[1])

    h = xc2

    print(" h : %f " % (h))
    print(" k : %f " % (k))
    #k = rearbumpercoordinates[0][1] - r + 0.5*width + abs(distance[0])
    #k = freespacecoordinates[0][0][1] + distance[0]
    pointsofinterest.append([h,k2,0])

    x = h
    while (-r < x - h < r ):
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = -(1)*x + 2*h
        y = (1) * (math.sqrt(pow(r, 2) - pow((x - h), 2)))
        y = y + k2
        pose.pose.position.y = y
        #print(" x : %f " % (x))
        #print(" y : %f " % (y))
        if (pose.pose.position.x >= xt):
            initial.append([pose.pose.position.x, pose.pose.position.y])
            #pathofcar.poses.append(pose)
        x += 0.1

    i = len(initial)-1
    while(i>=0):
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = initial[i][0]
        pose.pose.position.y = initial[i][1]
        pathofcar.poses.append(pose)
        i -=1
    #scaled_polygon_pcl = pcl2.create_cloud_xyz32(header, rearbumpercoordinates[0 : 25])
    scaled_polygon_pcl = pcl2.create_cloud_xyz32(header, pointsofinterest)

    pc_pub.publish(scaled_polygon_pcl)
    path_publisher.publish(pathofcar)
    rospy.loginfo("happily publishing sample pointcloud.. !")
    print(initial)
    print(len(initial))
    print("Destination")
    print(destinationpoints)
    print(len(destinationpoints))


    countflag = 2
if __name__ == "__main__":
    main()
